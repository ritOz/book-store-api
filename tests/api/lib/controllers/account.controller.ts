import { ApiRequest } from "../request";
let baseUrl = global.appConfig.baseUrl;

export class AccountController {
  async addUser(userSettings: object) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`Account/v1/User`)
      .body(userSettings)
      .send();
    return response;
  }
  async generateToken(userSettings: object) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`Account/v1/GenerateToken`)
      .body(userSettings)
      .send();
    return response;
  }
  async authorizedUser(userSettings: object) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`Account/v1/Authorized`)
      .body(userSettings)
      .send();
    return response;
  }
  async findUserById(userID: string, accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .bearerToken(accessToken)
      .method("GET")
      .url(`Account/v1/User/${userID}`)
      .send();
    return response;
  }
  async deleteUserById(userID: string, accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .bearerToken(accessToken)
      .method("DELETE")
      .url(`Account/v1/User/${userID}`)
      .send();
    return response;
  }
}
