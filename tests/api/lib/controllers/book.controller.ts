import { ApiRequest } from "../request";
let baseUrl = global.appConfig.baseUrl;

export class BookController {
  async getAllBooks() {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`BookStore/v1/Books`)
      .send();
    return response;
  }
  async addListOfBooks(userList: object, accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .bearerToken(accessToken)
      .method("POST")
      .url(`BookStore/v1/Books`)
      .body(userList)
      .send();
    return response;
  }
  async deleteListOfBooks(userId: string, accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .bearerToken(accessToken)
      .method("DELETE")
      .url(`BookStore/v1/Books?UserId=${userId}`)
      .send();
    return response;
  }
  async getBook(bookISBN: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`BookStore/v1/Book?ISBN=${bookISBN}`)
      .send();
    return response;
  }
  async deleteBookFromList(userSettings: object, accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .bearerToken(accessToken)
      .method("DELETE")
      .url(`BookStore/v1/Book`)
      .body(userSettings)
      .send();
    return response;
  }
  async replaceBook(bookISBN: string, userSettings: object, accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .bearerToken(accessToken)
      .method("PUT")
      .url(`BookStore/v1/Books/${bookISBN}`)
      .body(userSettings)
      .send();
    return response;
  }
}
