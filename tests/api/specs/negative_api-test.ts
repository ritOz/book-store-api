import { userInfo } from 'os';
import { AccountController } from '../lib/controllers/account.controller';
import { BookController } from '../lib/controllers/book.controller';
import { expect } from 'chai';
import { checkStatusCode, checkResponseBodyCode, checkResponseBodyResult, checkResponseBody, checkResponseBodyType, checkResponseTime, checkSchema, checkResponseBodyMessage, checkArticlesAmount, checkResponseBodyStatus } from '../../helpers/functionsForChecking.helpers';

const account = new AccountController();
const book = new BookController();
const schemas = require('./data/schemas_testData.json');
let chai = require('chai');
chai.use(require('chai-json-schema'));

let userID: string, token, firstBookISBN, secondBookISBN;
let userData = global.appConfig.userData;

describe('Negative tests', () => {
  let emptyCredentialsDataSet = [
    { userName: '', password: '' },
    { userName: userData.userName, password: '' },
    { userName: '', password: userData.password}
  ];

  let invalidCredentialsDataSet = [
    { userName: userData.userName, password: ' '},
    { userName: userData.userName, password: '1@We' },
    { userName: userData.userName, password: 'wzxcddgz' },
    { userName: userData.userName, password: '12345678' },
    { userName: userData.userName, password: 'WZXCDDGZ' },
    { userName: userData.userName, password: '!@#$%^&*' },
    { userName: userData.userName, password: 'wzxcDGZA' },
    { userName: userData.userName, password: 'wzxcdDGZ1' },
    { userName: userData.userName, password: 'wzxcdDGZ!' }
  ];

  emptyCredentialsDataSet.forEach((credentials) => {
    it(`Registration with one of or both empty felds`, async() => {
      let response = await account.addUser(credentials);
      checkStatusCode(response, 400);
      checkResponseBodyMessage(response, 'UserName and Password required.');
      checkResponseBodyCode(response, '1200');
      checkSchema(response, schemas.schema_errorForEmptyFields);
    })
  })

  invalidCredentialsDataSet.forEach((credentials) => {
    it(`Registration with invalid credentials`, async() => {
      let response = await account.addUser(credentials);
      checkStatusCode(response, 400);
      checkResponseBodyMessage(response, "Passwords must have at least one non alphanumeric character, one digit ('0'-'9'), one uppercase ('A'-'Z'), one lowercase ('a'-'z'), one special character and Password must be eight characters or longer.");
      checkResponseBodyCode(response, '1300');
      checkSchema(response, schemas.schema_errorForEmptyFields);
    })
  })

  before(`Register user`, async () => {
    let response = await account.addUser(userData);
    userID = response.body.userID;
  });

  it(`Registering an already registered user`, async () => {
    let response = await account.addUser(userData);
    checkStatusCode(response, 406);
    checkResponseBodyMessage(response, 'User exists!');
    checkResponseBodyCode(response, '1204');
    checkSchema(response, schemas.schema_errorForEmptyFields);
  })

  before(`Get token `, async () => {
    let response = await account.generateToken(userData);
    token = response.body.token;
  });

  emptyCredentialsDataSet.forEach((credentials) => {
    it(`Authorization with one of or both empty felds`, async () => {
      let response = await account.authorizedUser(credentials);
      checkStatusCode(response, 400);
      checkResponseBodyMessage(response, 'UserName and Password required.');
      checkResponseBodyCode(response, '1200');
      checkSchema(response, schemas.schema_errorForEmptyFields);
    })
  })

  invalidCredentialsDataSet.forEach((credentials) => {
    it(`Authorization with invalid password`, async () => {
      let response = await account.authorizedUser(credentials);
      checkStatusCode(response, 404);
      checkResponseBodyMessage(response, "User not found!");
      checkResponseBodyCode(response, '1207');
      checkSchema(response, schemas.schema_errorForEmptyFields);
    })
  })

  it('Authorization with true password and wrong userName', async () => {
    let userSettings = {
      userName: userData.userName + 1,
      password: userData.password
    }
    let response = await account.authorizedUser(userSettings);
    checkStatusCode(response, 404);
    checkResponseBodyMessage(response, 'User not found!');
    checkResponseTime(response, 5000);
  })

  emptyCredentialsDataSet.forEach((credentials) => {
    it(`Generate token with one of or both empty felds`, async () => {
      let response = await account.generateToken(credentials);
      checkStatusCode(response, 400);
      checkResponseBodyMessage(response, 'UserName and Password required.');
      checkResponseBodyCode(response, '1200');
      checkSchema(response, schemas.schema_errorForEmptyFields);
    })
  })

  invalidCredentialsDataSet.forEach((credentials) => {
    it(`Generate token with invalid password`, async () => {
      let response = await account.generateToken(credentials);
      checkStatusCode(response, 200);
      checkResponseBodyStatus(response, 'Failed');
      checkResponseBodyResult(response, 'User authorization failed.')
      checkSchema(response, schemas.schema_generateToken);
    })
  })

  it('Generate token with true password and wrong userName', async () => {
    let userSettings = {
      userName: userData.userName + 1,
      password: userData.password
    }
    let response = await account.generateToken(userSettings);
    checkStatusCode(response, 200);
    checkResponseBodyStatus(response, 'Failed');
    checkResponseBodyResult(response, 'User authorization failed.')
    checkSchema(response, schemas.schema_generateToken);
  })

  it('Find user by ID with wrong token', async () => {
    let response = await account.findUserById(userID, token +1);
    checkStatusCode(response, 401);
    checkResponseBodyMessage(response, 'User not authorized!');
    checkResponseBodyCode(response, '1200');
    checkSchema(response, schemas.schema_errorForEmptyFields);
    checkResponseTime(response, 5000);
  })

  it('Find user by ID with wrong ID and true token', async () => {
    let response = await account.findUserById(userID +1 , token);
    checkStatusCode(response, 401);
    checkResponseBodyMessage(response, 'User not found!');
    checkResponseBodyCode(response, '1207');
    checkSchema(response, schemas.schema_errorForEmptyFields);
    checkResponseTime(response, 5000);
  })

  it('Delete user by ID with wrong token', async () => {
    let response = await account.deleteUserById(userID, token + 1);
    checkStatusCode(response, 401);
    checkResponseBodyMessage(response, 'User not authorized!');
    checkResponseBodyCode(response, '1200');
    checkSchema(response, schemas.schema_errorForEmptyFields);
    checkResponseTime(response, 5000);
  })

  it('Delete user by ID with wrong ID and true token', async () => {
    let response = await account.deleteUserById(userID + 1, token);
    checkStatusCode(response, 200);
    checkResponseBodyMessage(response, 'User Id not correct!');
    checkResponseBodyCode(response, '1207');
    checkSchema(response, schemas.schema_errorForEmptyFields);
    checkResponseTime(response, 5000);
  })

  it('Get book by wrong ISBN', async () => {
    let response = await book.getBook('example');
    checkStatusCode(response, 400);
    checkResponseTime(response, 5000);
    checkResponseBodyMessage(response, 'ISBN supplied is not available in Books Collection!');
    checkResponseBodyCode(response, '1205');
    checkSchema(response, schemas.schema_errorForEmptyFields);
  })

  before('Get all books', async () => {
    let response = await book.getAllBooks();
    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkSchema(response, schemas.schema_getAllBooks);
    firstBookISBN = response.body.books[0].isbn;
    secondBookISBN = response.body.books[1].isbn;
  })

  it('Create userList with wrong userID', async () => {
    let userList = {
      "userId": userID + 1,
      "collectionOfIsbns": [
        { "isbn": firstBookISBN },
        { "isbn": secondBookISBN}
      ]
    }
    let response = await book.addListOfBooks(userList, token);
    checkStatusCode(response, 401);
    checkResponseTime(response, 5000);
    checkResponseBodyMessage(response, 'User Id not correct!');
    checkResponseBodyCode(response, '1207');
    checkSchema(response, schemas.schema_errorForEmptyFields);
  })

  it('Create userList with wrong token', async () => {
    let userList = {
      "userId": userID,
      "collectionOfIsbns": [
        { "isbn": firstBookISBN },
        { "isbn": secondBookISBN }
      ]
    }
    let response = await book.addListOfBooks(userList, token +1);
    checkStatusCode(response, 401);
    checkResponseTime(response, 5000);
    checkResponseBodyMessage(response, 'User not authorized!');
    checkResponseBodyCode(response, '1200');
    checkSchema(response, schemas.schema_errorForEmptyFields);
  })
  
  it('Delete book from userList with wrong ID', async () => {
    let userSettings = {
      "isbn": firstBookISBN,
      "userId": userID+1
    }
    let response = await book.deleteBookFromList(userSettings, token);
    checkStatusCode(response, 401);
    checkResponseTime(response, 5000);
    checkResponseBodyMessage(response, 'User Id not correct!');
    checkResponseBodyCode(response, '1207');
    checkSchema(response, schemas.schema_errorForEmptyFields);
  })

  it('Delete book from userList with wrong isbn', async () => {
    let userSettings = {
      "isbn": firstBookISBN +1,
      "userId": userID
    }
    let response = await book.deleteBookFromList(userSettings, token);
    checkStatusCode(response, 400);
    checkResponseTime(response, 5000);
    checkResponseBodyMessage(response, "ISBN supplied is not available in User's Collection!");
    checkResponseBodyCode(response, '1206');
    checkSchema(response, schemas.schema_errorForEmptyFields);
  })

  it('Delete book from userList with wrong token', async () => {
    let userSettings = {
      "isbn": firstBookISBN,
      "userId": userID
    }
    let response = await book.deleteBookFromList(userSettings, token +1);
    checkStatusCode(response, 401);
    checkResponseTime(response, 5000);
    checkResponseBodyMessage(response, 'User not authorized!');
    checkResponseBodyCode(response, '1200');
    checkSchema(response, schemas.schema_errorForEmptyFields);
  })

  it('Replace book in a list with wrong userID', async () => {
    let userSettings = {
      "userId": userID +1,
      "isbn": firstBookISBN
    }
    let response = await book.replaceBook(secondBookISBN, userSettings, token);
    checkStatusCode(response, 401);
    checkResponseTime(response, 5000);
    checkResponseBodyMessage(response, 'User Id not correct!');
    checkResponseBodyCode(response, '1207');
    checkSchema(response, schemas.schema_errorForEmptyFields);
  })

  it('Replace book in a list with wrong Books Collection isbn', async () => {
    let userSettings = {
      "userId": userID,
      "isbn": firstBookISBN +1
    }
    let response = await book.replaceBook(secondBookISBN, userSettings, token);
    checkStatusCode(response, 400);
    checkResponseTime(response, 5000);
    checkResponseBodyMessage(response, 'ISBN supplied is not available in Books Collection!');
    checkResponseBodyCode(response, '1205');
    checkSchema(response, schemas.schema_errorForEmptyFields);
  })

  it('Replace book in a list with wrong User`s Collection isbn', async () => {
    let userSettings = {
      "userId": userID,
      "isbn": firstBookISBN
    }
    let response = await book.replaceBook(secondBookISBN + 1, userSettings, token);
    checkStatusCode(response, 400);
    checkResponseTime(response, 5000);
    checkResponseBodyMessage(response, "ISBN supplied is not available in User's Collection!");
    checkResponseBodyCode(response, '1206');
    checkSchema(response, schemas.schema_errorForEmptyFields);
  })

  it('Replace book in a list with wrong token', async () => {
    let userSettings = {
      "userId": userID,
      "isbn": firstBookISBN
    }
    let response = await book.replaceBook(secondBookISBN, userSettings, token + 1);
    checkStatusCode(response, 401);
    checkResponseTime(response, 5000);
    checkResponseBodyMessage(response, 'User not authorized!');
    checkResponseBodyCode(response, '1200');
    checkSchema(response, schemas.schema_errorForEmptyFields);
  })

  it('Delete userList of books with wrong userID', async () => {
    let response = await book.deleteListOfBooks(userID +1, token);
    checkStatusCode(response, 401);
    checkResponseTime(response, 5000);
    checkResponseBodyMessage(response, 'User Id not correct!');
    checkResponseBodyCode(response, '1207');
    checkSchema(response, schemas.schema_errorForEmptyFields);
  })

  it('Delete userList of books with wrong token', async () => {
    let response = await book.deleteListOfBooks(userID, token +1);
    checkStatusCode(response, 401);
    checkResponseTime(response, 5000);
    checkResponseBodyMessage(response, 'User not authorized!');
    checkResponseBodyCode(response, '1200');
    checkSchema(response, schemas.schema_errorForEmptyFields);
  })

  after(`Delete user`, async () => {
    let response = await account.deleteUserById(userID, token);
  });
})