import { AccountController } from '../lib/controllers/account.controller';
import { BookController } from '../lib/controllers/book.controller';
import { expect } from 'chai';
import { checkStatusCode, checkResponseBodyResult, checkResponseBody, checkResponseBodyType, checkResponseTime, checkSchema, checkResponseBodyMessage, checkArticlesAmount, checkResponseBodyStatus } from '../../helpers/functionsForChecking.helpers';

const account = new AccountController();
const book = new BookController();
const schemas = require('./data/schemas_testData.json');
let chai = require('chai');
chai.use(require('chai-json-schema'));

let userID, token, firstBookISBN, secondBookISBN;
let userData = global.appConfig.userData;
 
describe('Positive tests', () => {
  it('Create new user', async () => {
    let response = await account.addUser(userData);
    checkStatusCode(response, 201);
    checkResponseTime(response, 5000);
    checkSchema(response, schemas.schema_createUser);
    userID = response.body.userID;
  })

  it('Generate token', async () => {
    let response = await account.generateToken(userData);
    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkResponseBodyResult(response, 'User authorized successfully.');
    checkResponseBodyStatus(response, 'Success')
    checkSchema(response, schemas.schema_generateToken);
    token = response.body.token;    
  })

  it('Check authorization', async () => {
    let response = await account.authorizedUser(userData);
    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkResponseBody(response, true);
    checkSchema(response, schemas.schema_authorization);
  })

  it('Find user by ID', async () => {
    let response = await account.findUserById(userID, token);
    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkSchema(response, schemas.schema_findUserById);
  })

  it('Get all books', async () => {
    let response = await book.getAllBooks();
    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkSchema(response, schemas.schema_getAllBooks);
    firstBookISBN = response.body.books[0].isbn;
    secondBookISBN = response.body.books[1].isbn;
  })

  it('Get book by ISBN', async () => {
    let response = await book.getBook(firstBookISBN);
    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkSchema(response, schemas.schema_getBookByISBN);
  })

  it('Create userList', async () => {
    let userList = {
      "userId": userID,
      "collectionOfIsbns": [
        { "isbn": firstBookISBN },
        { "isbn": secondBookISBN }
      ]
    }
    let response = await book.addListOfBooks(userList, token);
    checkStatusCode(response, 201);
    checkResponseTime(response, 5000);
    checkSchema(response, schemas.schema_addListOfBooks);
  })

  it('Delete book from userList', async () => {
    let userSettings = {
      "userId": userID,
      "isbn": firstBookISBN
    }
    let response = await book.deleteBookFromList(userSettings, token);
    checkStatusCode(response, 204);
    checkResponseTime(response, 5000);
  })
  
  it('Replace book in a list', async () => {
    let userSettings = {
      "userId": userID,
      "isbn": firstBookISBN
    }
    let response = await book.replaceBook(secondBookISBN, userSettings, token);
    checkStatusCode(response, 200);
    checkResponseTime(response, 5000);
    checkSchema(response, schemas.schema_replaceBookInUserList);
  })

  it('Delete userList of books', async () => {
    let response = await book.deleteListOfBooks(userID, token);
    checkStatusCode(response, 204);
    checkResponseTime(response, 5000);
  })
  
  it('Delete user by ID', async () => {
    let response = await account.deleteUserById(userID, token);
    checkStatusCode(response, 204);
    checkResponseTime(response, 5000);
  })
})
